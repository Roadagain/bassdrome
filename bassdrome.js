function replay(audio){
    if (!audio.paused){
        // 再生中なら一度止めて巻き戻し
        audio.pause();
        audio.currentTime = 0;
    }
    audio.play();
}

window.addEventListener('load', function(){
    var start = document.getElementById('start');
    var stop = document.getElementById('stop');
    var kicks = document.getElementById('kicks');
    var kick = [];
    var intervalId;

    start.addEventListener('click', function(){
        var bpm = document.getElementById('bpm').value;
        var separate = document.getElementById('separate').value;
        var interval = 1000 * 240 / bpm / separate;
        var beat = (500 / interval | 0) + 1; //0.5秒のキック回数(小数点以下切り捨てのためマージンで+1)
        var current = 0;

        clearInterval(intervalId);
        //足りない数のキックを追加して読み込む
        for (var i = kicks.childNodes.length; i < beat; i++){
            kick[i] = kicks.appendChild(document.createElement('audio'));
            kick[i].src = 'kick.mp3';
            kick[i].load();
        }

        //複数個のキックを順番ずつ再生する
        //TODO: 速い時のズレを解消
        intervalId = window.setInterval(function(){
            replay(kick[current]);
            current = (current + 1) % beat;
        }, interval);
    });
    stop.addEventListener('click', function(){
        window.clearInterval(intervalId);
    });
});
